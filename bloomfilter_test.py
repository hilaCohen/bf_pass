from bloomfilter import BloomFilter

print("Hello and Welcome to our passwords app")
print("We will start with some configuration")
n = int(input("Please enter the size of array bits: "))
k = int(input("Please enter the number of hash functions: "))
# configuration
bloomf = BloomFilter(n, k)


print("\n", "Below you will find a several options: ")
menu = {'1': "Show Bloom Filter array of bit", '2': "Update guessable passwords list", '3': "Add new password",
        '4': "Check existence of password ", '5': "False Positive statistic", '6': "Check password strength",
        '7': "Exit"}

# user menu
while True:
    options = menu.keys()
    for entry in options:
        print(entry, menu[entry])

    selection = input("Please Select An Option : ")
    if selection == '1':
        # Show Bloom Filter array of bit
        bloomf.print_array(bloomf.bit_array)

    elif selection == '2':
        # Update guessable passwords list"
        password = input("Please enter a password to update: ")
        bloomf.update_passwords_list(password)

    elif selection == '3':
        # Add new password
        password = input("Please enter a new password:  ")
        bloomf.add_new_password(password)

    elif selection == '4':
        # Check if password exist
        password = input("Please enter a password:  ")
        bloomf.check_exist_password(password)

    elif selection == '5':
        # False Positive statistic
        bloomf.false_positive()

    elif selection == '6':
        # Check password strength
        password = input("Please enter password:  ")
        print(bloomf.password_score(password), "%")

    elif selection == '7':
        # Exit
        break

    else:
        # invalid input
        print("Unknown Option Selected!")
