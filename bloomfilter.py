import math

import mmh3
from bitarray import bitarray


class BloomFilter(object):

    def __init__(self, N, k):

        # Size of array of bit
        self.N = N
        # number of hash functions
        self.k = k

        self.bit_array = bitarray(self.N)

        # initialize all bits as 0
        self.bit_array.setall(0)
        self.numberOfAddedPasswords = 0
        self.bad_passwords_list = []
        self.fp = 0

    def initialize(self, N, k):
        # Size of bit array
        self.N = N
        # number of hash functions
        self.k = k
        # Bit array of given size
        self.bit_array = bitarray(self.N)
        # initialize all bits as 0
        self.bit_array.setall(0)
        self.numberOfAddedElements = 0
        self.bad_passwords_list = []

    def get_array(self):
        return self.bit_array

    def print_array(self, bit_array):
        for i in range(self.N):
            print(i, "\t", self.bit_array[i])

    def update_passwords_list(self, password):
        # add password to list
        digests = []
        inBadList = False
        if password not in self.bad_passwords_list:
            self.bad_passwords_list.insert(0, password)
            self.numberOfAddedPasswords += 1
            for i in range(self.k):
                digest = mmh3.hash(password, i) % self.N
                digests.append(digest)
                # turn on index value
                self.bit_array[digest] = True
            inBadList = True
        print("The updated list:")
        self.print_passwords_list()
        return inBadList

    def print_passwords_list(self):
        for password in range(len(self.bad_passwords_list)):
            print(self.bad_passwords_list[password], " ")

    def add_new_password(self, password):
        count_hash_collision = 0
        digests = []

        # check if already exist
        for i in range(self.k):
            digest = mmh3.hash(password, i) % self.N

            if self.bit_array[digest]:
                # if any of bit is True there is probability that it exist
                count_hash_collision += 1

        print("There was a collision with", count_hash_collision, "hash functions.")

        if count_hash_collision < self.k:
            # not exist - then add
            # optional to add additional conditon to add -  if score>60 :
            self.bad_passwords_list.insert(0, password)
            self.numberOfAddedPasswords += 1
            score = self.password_score(password)
            print("Password strength score: ", score, "%")

            # add to bloom filter
            for i in range(self.k):
                digest = mmh3.hash(password, i) % self.N
                digests.append(digest)
                # turn on index value
                self.bit_array[digest] = True

        if count_hash_collision == self.k and password not in self.bad_passwords_list:
            # false positive!!!
            self.fp += 1

        return count_hash_collision

    def check_exist_password(self, password):
        s = []
        count_bits = 0

        for i in range(self.k):
            digest = mmh3.hash(password, i) % self.N
            if self.bit_array[digest]:
                count_bits += 1

        if count_bits == self.k:
            for i in range(self.k):
                digest = mmh3.hash(password, i) % self.N
                print("Hash function", i, " return ", digest)
                print("BloomFilter[", digest, "] = ", self.bit_array[digest])
                s.append(
                    f"Hash function number {i}  return {digest}. \n BloomFilter[ {digest} ] = {self.bit_array[digest]}.")

            s.insert(0, "All bits are on - The password exist in the system")
            print("All bits are on - The password exist in the system")

        else:
            print("The password do not exist ")
            s.append("The password do not exist ")

        return s

    def get_num_of_password_added(self):
        return self.numberOfAddedPasswords

    def false_positive(self):
        # extra calculating- not need
        n = self.get_num_of_password_added()
        counter = 0
        # num of true bits
        for i in self.bit_array:
            if self.bit_array[i]:
                counter += 1
        doubly = (n * self.k) - counter

        # ********After mailing with Norit - This was the intention: ****
        # from add new password function collect the mistaken rejected pass
        print("There was", self.fp, "errors of false positive passwords")
        return self.fp

    def password_score(self, password):
        score = 0
        special_chars = ['!', '@', '#', '$', '%', '^', '&', '*', '.', '?', '/', '_']

        req1 = False
        if 8 <= len(password) <= 14:
            # length should be at least 8 and max 14
            req1 = True
            score += 20

        req2 = False
        if any(char.isdigit() for char in password):
            # password should have at least one numeral
            req2 = True
            score += 15

        req3 = False
        if any(char.isupper() for char in password):
            # password should have at least one uppercase letter
            req3 = True
            score += 15

        req4 = False
        if any(char.islower() for char in password):
            # password should have at least one lowercase letter
            req4 = True
            score += 15

        req5 = False
        if any(char in special_chars for char in password):
            # password should have at least one of the symbols
            req5 = True
            score += 15

        if req1 and req2 and req3 and req4 and req5:
            score += 5

        if len(set(password)) == len(password):
            # unique char in password
            score += 15

        if all(char.isdigit() for char in password):
            # password should not contain only numbers
            score -= 5

        if all(char.isalpha() for char in password):
            # password should not contain only letters
            score -= 5

        if 1 <= len(password) < 8:
            # length should be at least 8
            score -= 10

        if not req1 and not req2 and not req3 and not req4 and not req5:
            score -= 5

        if score < 0:
            score = 0
        return score
