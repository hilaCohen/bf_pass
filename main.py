import tkinter as tk
from bloomfilter import BloomFilter

# random initialize
bloomf = BloomFilter(0, 0)


# ------------------------------
def resetConsole():
    mylist.pack_forget()
    mylist.delete(0, "end")
    c.delete("all")
    c.pack_forget()


def clearUserPassword():
    PASSWORDentry.delete(0, 'end')


def checkForNumber(userString):
    # check input validity
    try:
        int(userString)
        return True
    except ValueError:
        return False


def letsGoClicked():
    # bloom filter configuration
    if checkForNumber(Nentry.get()) & checkForNumber(Kentry.get()):
        # Set the options menu
        Optionsframe.place(relx=0.5, rely=0.51, anchor="c")
        label['text'] = f"INITIALIZE:\n N = {Nentry.get()}, K = {Kentry.get()}"
        # Init the bloom filter object
        bloomf.initialize(int(Nentry.get()), int(Kentry.get()))
    else:
        label['text'] = "Invalid Input. Please enter only numbers for N and K"


def option1Clicked():
    # print array of bits
    resetConsole()
    n_array = bloomf.get_array()
    for i, val in enumerate(n_array):
        string = f"BloomFilter [{i}] = {val}"
        mylist.insert("end", string)

    mylist.pack(side='left')


def option2Clicked():
    # update bad passwords list
    resetConsole()
    if PASSWORDentry.get():
        userPassword = PASSWORDentry.get()
        if bloomf.update_passwords_list(userPassword):
            label['text'] = f"The Password {userPassword} was added successfully to the Bad list  "
        else:
            label['text'] = f"Your Password {userPassword} ia already in the Bad list!"
    else:
        label['text'] = "Please Enter a password"
    clearUserPassword()


def option3Clicked():
    # add new password
    resetConsole()
    if PASSWORDentry.get():
        userPassword = PASSWORDentry.get()
        collision = bloomf.add_new_password(userPassword)
        label['text'] = f"There was a collision with {collision} hash functions. \n\n"
        if collision < bloomf.k:
            score = bloomf.password_score(userPassword)
            label['text'] += f"Password pass filter with strength score : {score}%"
            passswordStrong = 5 * score
            if passswordStrong <= 125:
                color_fill = "red"
            elif passswordStrong <= 250:
                color_fill = "yellow"
            else:
                color_fill = "green"
            c.create_rectangle(3, 3, 500, 25, fill='black')
            c.create_rectangle(3, 3, passswordStrong, 25, fill=color_fill)

        c.pack(side='bottom')
    else:
        label['text'] = "Please Enter a password"
    clearUserPassword()


def option4Clicked():
    # check exist password
    resetConsole()
    if PASSWORDentry.get():
        userPassword = PASSWORDentry.get()
        print("dfdf")
        exist_array = bloomf.check_exist_password(userPassword)
        for val in exist_array:
            mylist.insert("end", val)

        mylist.pack(side='left')
    else:
        label['text'] = "Please Enter a password"
    clearUserPassword()


def option5Clicked():
    mylist.pack_forget()
    c.delete("all")
    c.pack_forget()
    label['text'] = f"There was {bloomf.false_positive()} errors of false positive "
    clearUserPassword()


def option6Clicked():
    resetConsole()
    if PASSWORDentry.get():
        userPassword = PASSWORDentry.get()
        passswordStrong = 5 * bloomf.password_score(userPassword)
        if passswordStrong <= 125:
            color_fill = "red"
        elif passswordStrong <= 250:
            color_fill = "yellow"
        else:
            color_fill = "green"
        label['text'] = f"\nPassword strength score: {bloomf.password_score(userPassword)}%"
        c.create_rectangle(3, 3, 500, 25, fill='black')
        c.create_rectangle(3, 3, passswordStrong, 25, fill=color_fill)
        c.pack(side='bottom')
    else:
        label['text'] = "Please Enter a password"

    clearUserPassword()



def option7Clicked():
    # print list of passwords
    resetConsole()
    bad_list = bloomf.bad_passwords_list
    for i, val in enumerate(bad_list):
        string = f"Password number {i+1} : {val}"
        mylist.insert("end", string)

    mylist.pack(side='left')


# ------------------------------


root = tk.Tk()  # Toplevel()
root.title('Hila Cohen & Yuval Atia - Bloom Filter')
w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (w, h))

background_image = tk.PhotoImage(file='background.gif')
background_label = tk.Label(root, image=background_image)
background_label.place(relwidth=1, relheight=1)

# ---------------------------------------

frame = tk.Frame(root, bg='#80c1ff', bd=2)
frame.place(relx=0.5, rely=0.15, anchor="c")

logo_image = tk.PhotoImage(file='logo.gif')
logo_label = tk.Label(frame, image=logo_image)
logo_label.grid(row=0, column=0, columnspan=2)

Nlabel = tk.Label(frame, text="Enter N - Size Of Array : ", bg='#80c1ff')
Nlabel.config(font=("Ariel", 13))
Nlabel.grid(row=1, column=0)
Nentry = tk.Entry(frame)
Nentry.grid(row=1, column=1)

Klabel = tk.Label(frame, text="Enter K - Num Of Hash Functions : ", bg='#80c1ff')
Klabel.config(font=("Ariel", 13))
Klabel.grid(row=2, column=0)
Kentry = tk.Entry(frame)
Kentry.grid(row=2, column=1)

InitializeButton = tk.Button(frame, text="configure >", font=30, bd=1, bg='#80c1ff',
                             command=lambda: letsGoClicked())
InitializeButton.grid(row=3, column=0, columnspan=2)

# ------------------------------------------------
Optionsframe = tk.Frame(root, bg='#80c1ff', bd=2)


Passwordframe = tk.Frame(Optionsframe, bg='#80c1ff')
Passwordframe.grid(row=3)
PASSWORDlabel0 = tk.Label(Passwordframe, text="", bg='#80c1ff')
PASSWORDlabel0.pack()
PASSWORDlabel = tk.Label(Passwordframe, text="Enter A Password: ", bg='#80c1ff')
PASSWORDlabel.config(font=("Ariel", 10))
PASSWORDlabel.pack()
PASSWORDentry = tk.Entry(Passwordframe)
PASSWORDentry.pack()
PASSWORDlabel2 = tk.Label(Passwordframe, text="", bg='#80c1ff')
PASSWORDlabel2.pack()
Option1_Button = tk.Button(Optionsframe, text="Show Bloom Filter array of bit", width=40, height=1, compound="c", font=40, bd=1, bg='blue',
                           command=lambda: option1Clicked())
Option1_Button.grid(row=0, )
Option2_Button = tk.Button(Optionsframe, text="Update guessable passwords list", width=40, height=1, compound="c", font=40, bd=1, bg='#80c1ff',
                           command=lambda: option2Clicked())
Option2_Button.grid(row=4)
Option3_Button = tk.Button(Optionsframe, text="Add new password", width=40, height=1, compound="c", font=40, bd=1, bg='#80c1ff',
                           command=lambda: option3Clicked())
Option3_Button.grid(row=5)
Option4_Button = tk.Button(Optionsframe, text="Check existence of password", width=40, height=1, compound="c", font=40, bd=1, bg='#80c1ff',
                           command=lambda: option4Clicked())
Option4_Button.grid(row=6)
Option5_Button = tk.Button(Optionsframe, text="False Positive statistic", width=40, height=1, compound="c", font=40, bd=1, bg='#80c1ff',
                           command=lambda: option5Clicked())
Option5_Button.grid(row=2)
Option6_Button = tk.Button(Optionsframe, text="Check password strength", width=40, height=1, compound="c", font=40, bd=1, bg='#80c1ff',
                           command=lambda: option6Clicked())
Option6_Button.grid(row=7)

Option7_Button = tk.Button(Optionsframe, text="Show list of bad passwords", width=40, height=1, compound="c", font=40, bd=1, bg='blue',
                           command=lambda: option7Clicked())
Option7_Button.grid(row=1)

# ------------------------------------------------

lower_frame = tk.Frame(root, bd=10)
lower_frame.place(relx=0.5, rely=0.89, relwidth=1, relheight=0.28, anchor='c')


label = tk.Label(lower_frame)
label.config(font=("Courier", 12))
label.place(relwidth=1)

c = tk.Canvas(lower_frame, height=100, width=500)

scrollbar = tk.Scrollbar(lower_frame)
scrollbar.pack(side='right', fill='y')

mylist = tk.Listbox(lower_frame, yscrollcommand=scrollbar.set, height=500, width=500)
mylist.config(font=("Courier", 12), justify="center")

# ------------------------------------------------

root.mainloop()
